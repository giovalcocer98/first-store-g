                                                                       
                                                                       
,------.,--.               ,--.    ,---.   ,--.                        
|  .---'`--',--.--. ,---.,-'  '-. '   .-',-'  '-. ,---. ,--.--. ,---.  
|  `--, ,--.|  .--'(  .-''-.  .-' `.  `-.'-.  .-'| .-. ||  .--'| .-. : 
|  |`   |  ||  |   .-'  `) |  |   .-'    | |  |  ' '-' '|  |   \   --. 
`--'    `--'`--'   `----'  `--'   `-----'  `--'   `---' `--'    `----' 
${application.title} ${application.version}
Powered by Spring Boot ${spring-boot.version}
Implemented by ENDE TECNOLOGIAS
                                                                       
                                                                       