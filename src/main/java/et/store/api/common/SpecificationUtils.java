package et.store.api.common;

import org.springframework.data.jpa.domain.Specification;
import jakarta.persistence.criteria.Predicate;
import java.util.Map;

public class SpecificationUtils {

    public static <T> Specification<T> createSpecification(Map<String, Map<String, Object>> filterFields) {
        Map<String, Object> likeFields =filterFields.get("likeFields");
        Map<String, Object> equalsFields=filterFields.get("equalsFields");
        return (root, query, builder) -> {
            Predicate predicate = builder.conjunction();

            if (likeFields != null){
                for (Map.Entry<String, Object> entry : likeFields.entrySet()) {
                    String fieldName = entry.getKey();
                    Object value = entry.getValue();

                    if (value instanceof String) {
                        predicate = builder.and(predicate, builder.like(builder.upper(root.get(fieldName)), "%" + ((String) value).toUpperCase() + "%"));
                    }
                }
            }

            if (equalsFields != null){
                for (Map.Entry<String, Object> entry : equalsFields.entrySet()) {
                    String fieldName = entry.getKey();
                    Object value = entry.getValue();

                    String[] parts = fieldName.split("\\.");
                    if (fieldName !=null &&parts.length == 2 && value != null) {
                        String relationName = parts[0];
                        String field = parts[1];
                        predicate = builder.and(predicate, builder.equal(root.get(relationName).get(field), value));
                    }
                    if (fieldName !=null &&parts.length == 1 && value != null && value != "") {
                        predicate = builder.and(predicate, builder.equal(root.get(fieldName), value));
                    }
                }
            }

            return predicate;
        };
    }

}
