package et.store.api.domain.service.interfaces;

import et.store.api.domain.entity.Category;
import et.store.api.presentation.request.dto.CategoryDto;
import et.store.api.presentation.response.pojo.CategoryProductPojo;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();

    Category create(CategoryDto category);

    Category update(Integer id, CategoryDto category);

    Category getById(Integer id);

    void delete(Integer id);

    CategoryProductPojo getByIdWithProducts(Integer id);
}
