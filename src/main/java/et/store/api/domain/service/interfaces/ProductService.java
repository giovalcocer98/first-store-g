package et.store.api.domain.service.interfaces;

import et.store.api.domain.entity.Product;
import et.store.api.presentation.request.dto.ProductDto;
import et.store.api.presentation.request.filter.ProductFilter;
import et.store.api.presentation.response.pojo.ProductPojo;

import java.util.List;

public interface ProductService  {
    List<Product> getAll();

    Product create(ProductDto product);

    Product update(Integer id, ProductDto product);

    Product getById(Integer id);

    void delete(Integer id);

    List<Product> getProductsByCategoryNative(Integer categoryId);

    List<Product> getProductsByCategoryCustomMethod(Integer categoryId);

    List<Product> getProductsByCategoryJPQL(Integer categoryId);

    List<ProductPojo> findByCategoryCustom(Integer categoryId);

    List<Product> getFiltered(String name, Integer stock);

    List<ProductPojo> customSearch(ProductFilter filters);
}
