package et.store.api.domain.service.implement;

import et.store.api.data.repository.CategoryRepository;
import et.store.api.data.repository.ProductRepository;
import et.store.api.domain.entity.Category;
import et.store.api.domain.mapper.CategoryMapper;
import et.store.api.domain.service.interfaces.CategoryService;
import et.store.api.exception.EntityNotFoundException;
import et.store.api.presentation.request.dto.CategoryDto;
import et.store.api.presentation.response.pojo.CategoryPojo;
import et.store.api.presentation.response.pojo.CategoryProductPojo;
import et.store.api.presentation.response.pojo.ProductPojo;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;
    private CategoryMapper categoryMapper;
    private ProductRepository productRepository;

    // private ProductService productService;



    @Override
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category create(CategoryDto dto) {
        Category category = categoryMapper.fromDto(dto, null);
        return categoryRepository.save(category);
    }

    @Override
    public Category update(Integer id, CategoryDto dto) {
        Category categoryFound = categoryRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Category", id));
        Category category = categoryMapper.fromDto(dto, categoryFound);
        return categoryRepository.save(category);
    }

    @Override
    public Category getById(Integer id) {
        Category category = categoryRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Category", id));
        return category;
    }

    @Override
    public void delete(Integer id) {
        Category category = categoryRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Category", id));
        categoryRepository.delete(category);
    }

    @Override
    public CategoryProductPojo getByIdWithProducts(Integer id) {
        CategoryProductPojo categoryProductPojo = new CategoryProductPojo();
        Category category = categoryRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Category", id));
        CategoryPojo categoryPojo = categoryRepository.findByIdCustom(category.getId());
        List<ProductPojo> productsPojo = productRepository.findByCategoryCustom(category);
        // List<ProductPojo> productsPojo = productService.findByCategoryCustom(category.getId());

        categoryProductPojo.setCategory(categoryPojo);
        categoryProductPojo.setProducts(productsPojo);
        return categoryProductPojo;
    }
}
