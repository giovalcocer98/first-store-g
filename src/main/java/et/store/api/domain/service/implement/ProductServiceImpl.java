package et.store.api.domain.service.implement;

import et.store.api.common.SpecificationUtils;
import et.store.api.data.repository.ProductRepository;
import et.store.api.domain.entity.Category;
import et.store.api.domain.entity.Product;
import et.store.api.domain.mapper.ProductMapper;
import et.store.api.domain.service.interfaces.CategoryService;
import et.store.api.domain.service.interfaces.ProductService;
import et.store.api.exception.EntityNotFoundException;
import et.store.api.presentation.request.dto.ProductDto;
import et.store.api.presentation.request.filter.ProductFilter;
import et.store.api.presentation.response.pojo.ProductPojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final CategoryService categoryService;

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @Override
    public Product create(ProductDto dto) {
        Product product = productMapper.fromDto(dto, null);
        Category category = categoryService.getById(dto.getCategoryId());
        product.setCategory(category);
        return productRepository.save(product);
    }

    @Override
    public Product update(Integer id, ProductDto dto) {
        Product productFound = productRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Product", id));
        Category category = categoryService.getById(dto.getCategoryId());
        Product product = productMapper.fromDto(dto, productFound);
        product.setCategory(category);
        return productRepository.save(product);
    }

    @Override
    public Product getById(Integer id) {
        Product product = productRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Product", id));
        return product;
    }

    @Override
    public void delete(Integer id) {
        Product product = productRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Product", id));
        productRepository.delete(product);
    }

    @Override
    public List<Product> getProductsByCategoryNative(Integer categoryId) {
        return  productRepository.findByCategoryId(categoryId);
    }

    @Override
    public List<Product> getProductsByCategoryCustomMethod(Integer categoryId) {
        Category category = categoryService.getById(categoryId);
        return  productRepository.findByCategory(category);
    }

    @Override
    public List<Product> getProductsByCategoryJPQL(Integer categoryId) {
        Category category = categoryService.getById(categoryId);
        return  productRepository.findByCategory(category);
    }

    @Override
    public List<ProductPojo> findByCategoryCustom(Integer categoryId) {
        Category category = categoryService.getById(categoryId);
        return productRepository.findByCategoryCustom(category);
    }

    @Override
    public List<Product> getFiltered(String name, Integer stock) {
        Map<String, Map<String, Object>> filterFields = new HashMap<>();

        Map<String, Object> likeFields = new HashMap<>();
        likeFields.put("name", name);
        likeFields.put("stock", stock);
        filterFields.put("likeFields", likeFields);

        Specification<Product> spec = SpecificationUtils.createSpecification(filterFields);
        List<Product> products = productRepository.findAll(spec);
        return products;
    }

    @Override
    public List<ProductPojo> customSearch(ProductFilter filters) {
        List<ProductPojo> products = productRepository.customSearch(filters);
        return products;
    }
}
