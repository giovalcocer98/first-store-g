package et.store.api.domain.mapper;

import et.store.api.presentation.request.dto.ProductDto;
import et.store.api.domain.entity.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {
    public Product fromDto(ProductDto dto, Product productFound) {
        Product product = new Product();
        if (productFound != null) {
            product = productFound;
        }
        product.setName(dto.getName());
        product.setDescription(dto.getDescription());
        product.setPrice(dto.getPrice());
        product.setStock(dto.getStock());
        return product;
    }
}
