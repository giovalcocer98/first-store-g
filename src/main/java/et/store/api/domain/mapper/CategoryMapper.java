package et.store.api.domain.mapper;

import et.store.api.presentation.request.dto.CategoryDto;
import et.store.api.domain.entity.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {

    public Category fromDto(CategoryDto dto, Category categoryFound) {
        Category category = new Category();
        if (categoryFound != null) {
            category = categoryFound;
        }
        category.setName(dto.getName());
        category.setDescription(dto.getDescription());
        return category;
    }
}
