package et.store.api.data.repository;

import et.store.api.domain.entity.Category;
import et.store.api.domain.entity.Product;
import et.store.api.presentation.request.filter.ProductFilter;
import et.store.api.presentation.response.pojo.ProductPojo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {
    @Query(value="SELECT * FROM products WHERE category_id = ?1", nativeQuery = true)
    List<Product> findByCategoryId(Integer categoryId);

    List<Product> findByCategory(Category category);

    @Query(value="SELECT p FROM Product p WHERE p.category = ?1")
    List<Product> findByCategoryJPQL(Category category);

    @Query("SELECT new et.store.api.presentation.response.pojo.ProductPojo(p.name, p.price, p.stock) FROM Product p WHERE p.category = ?1")
    List<ProductPojo> findByCategoryCustom(Category category);

    @Query("SELECT new et.store.api.presentation.response.pojo.ProductPojo(p.name, p.price, p.stock) FROM Product p WHERE p.category = ?1")
    List<ProductPojo> getFiltered();

    @Query("""
        SELECT new et.store.api.presentation.response.pojo.ProductPojo(p.name, p.price, p.stock) 
        FROM Product p 
        WHERE  
        (:#{#filter.name} IS NULL OR lower(p.name) LIKE lower(concat('%', :#{#filter.name}, '%') ))
        AND (:#{#filter.stock} IS NULL OR p.stock = :#{#filter.stock})
        """)
    List<ProductPojo> customSearch(@Param("filter") ProductFilter filter);
}