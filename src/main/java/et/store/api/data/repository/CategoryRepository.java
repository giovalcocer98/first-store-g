package et.store.api.data.repository;

import et.store.api.domain.entity.Category;
import et.store.api.presentation.response.pojo.CategoryPojo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    @Query("SELECT new et.store.api.presentation.response.pojo.CategoryPojo(c.name) FROM Category c WHERE c.id = ?1")
    CategoryPojo findByIdCustom(Integer categoryId);
}