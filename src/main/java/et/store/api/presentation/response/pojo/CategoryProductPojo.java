package et.store.api.presentation.response.pojo;

import et.store.api.common.Pojo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;

@Pojo
@Getter
@Setter
@NoArgsConstructor
public class CategoryProductPojo {

    private CategoryPojo category;
    private List<ProductPojo> products;
}
