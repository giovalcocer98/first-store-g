package et.store.api.presentation.response.pojo;

import et.store.api.common.Pojo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Pojo
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponsePojo {
    private String message;
    private Boolean value;
}
