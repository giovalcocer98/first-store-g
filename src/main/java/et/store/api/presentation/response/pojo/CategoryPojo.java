package et.store.api.presentation.response.pojo;

import et.store.api.common.Pojo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Pojo
@Getter
@Setter
@NoArgsConstructor
public class CategoryPojo {
    private String name;

    public CategoryPojo(String name) {
        this.name = name;
    }
}
