package et.store.api.presentation.response.pojo;

import et.store.api.common.Pojo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Pojo
@Getter
@Setter
@NoArgsConstructor
public class ProductPojo {

    private String name;
    private Double price;
    private Integer stock;

    public ProductPojo(String name, Double price, Integer stock) {
        this.name = name;
        this.price = price;
        this.stock = stock;
    }
}
