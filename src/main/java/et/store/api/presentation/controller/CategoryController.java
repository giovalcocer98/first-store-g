package et.store.api.presentation.controller;

import et.store.api.domain.entity.Product;
import et.store.api.exception.EntityNotFoundException;
import et.store.api.presentation.request.dto.CategoryDto;
import et.store.api.domain.entity.Category;
import et.store.api.presentation.response.pojo.CategoryProductPojo;
import et.store.api.domain.service.interfaces.CategoryService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import lombok.AllArgsConstructor;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@Tag(name = "Category Service", description = "Módulo para gestionar las categorías de la tienda")
@AllArgsConstructor
@RestController
@RequestMapping("/categories")
public class CategoryController {

    private final CategoryService categoryService;

    @Operation(
            description = "Endpoint que lista todas las categorías",
            responses = {
                    @ApiResponse(
                            description = "Response Ok",
                            responseCode = "200",
                            content = @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(
                                            schema = @Schema(implementation = Product.class)
                                    )
                            )
                    ),
                    @ApiResponse(
                            description = "Internal Server Error",
                            responseCode = "500",
                            content = @Content(
                                    mediaType = "plain/text",
                                    array = @ArraySchema(
                                            schema = @Schema(implementation = NullPointerException.class)
                                    )
                            )
                    )
            }
    )
    @GetMapping
    public ResponseEntity<List<Category>> getAll() {
        List<Category> categories = categoryService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(categories);
    }


    @Operation(
            description = "Endpoint que crea una categoría",
            responses = {
                    @ApiResponse(
                            description = "Response Ok",
                            responseCode = "200",
                            content = @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Product.class)
                            )
                    ),
                    @ApiResponse(
                            description = "Bad Request",
                            responseCode = "400",
                            content = @Content(
                                    mediaType = "plain/text",
                                    array = @ArraySchema(
                                            schema = @Schema(implementation = EntityNotFoundException.class)
                                    )
                            )
                    )
            }
    )
    @PostMapping
    public ResponseEntity<Category> create(@Valid @RequestBody CategoryDto dto) {
        Category categorySaved = categoryService.create(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(categorySaved);
    }

    @Operation(
            description = "Endpoint que actualiza una categoría",
            responses = {
                    @ApiResponse(
                            description = "Response Created",
                            responseCode = "201",
                            content = @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Product.class)
                            )
                    ),
                    @ApiResponse(
                            description = "Bad Request",
                            responseCode = "400",
                            content = @Content(
                                    mediaType = "plain/text",
                                    schema = @Schema(implementation = EntityNotFoundException.class)
                            )
                    )
            }
    )
    @PutMapping("/{id}")
    public ResponseEntity<Category> update(@PathVariable Integer id, @RequestBody CategoryDto dto) {
        dto.setId(id);
        Category categoryUpdated = categoryService.update(id, dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(categoryUpdated);
    }

    @Operation(
            description = "Endpoint que recupera una categoría",
            responses = {
                    @ApiResponse(
                            description = "Response Ok",
                            responseCode = "200",
                            content = @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Product.class)
                            )
                    )
            }
    )
    @GetMapping("/{id}")
    public ResponseEntity<Category> getById(@PathVariable Integer id) {
        Category categoryFound = categoryService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(categoryFound);
    }

    @Operation(
            description = "Endpoint que elimina una categoría",
            responses = {
                    @ApiResponse(
                            description = "Response Ok",
                            responseCode = "200",
                            content = @Content(
                                    mediaType = "application/json"
                            )
                    )
            }
    )
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Integer id) {
        categoryService.delete(id);
        return ResponseEntity.ok().build();
    }

    @Operation(
            description = "Endpoint que recupera una categoría con sus productos asociados",
            responses = {
                    @ApiResponse(
                            description = "Response Ok",
                            responseCode = "200",
                            content = @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(
                                            schema = @Schema(implementation = CategoryProductPojo.class)
                                    )
                            )
                    )
            }
    )
    @GetMapping("/withProducts/{id}")
    public ResponseEntity<CategoryProductPojo> getByIdWithProducts(@PathVariable Integer id) {
        CategoryProductPojo categoryWithProductsFound = categoryService.getByIdWithProducts(id);
        return ResponseEntity.status(HttpStatus.OK).body(categoryWithProductsFound);
    }
}
