package et.store.api.presentation.controller;

import et.store.api.domain.entity.Product;
import et.store.api.domain.service.interfaces.ProductService;
import et.store.api.presentation.request.dto.ProductDto;
import et.store.api.presentation.request.filter.ProductFilter;
import et.store.api.presentation.response.pojo.ProductPojo;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Tag(name = "Product Service", description = "Módulo para gestionar los productos de la tienda")
@AllArgsConstructor
@RestController
@RequestMapping("/products")
public class ProductController {
    private final ProductService productService;
    @GetMapping
    public ResponseEntity<List<Product>> getAll() {
        List<Product> products = productService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(products);
    }

    @PostMapping
    public ResponseEntity<Product> create(@Valid @RequestBody ProductDto dto) {
        Product productSaved = productService.create(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(productSaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> update(@PathVariable Integer id, @RequestBody ProductDto dto) {
        dto.setId(id);
        Product productUpdated = productService.update(id, dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(productUpdated);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getById(@PathVariable Integer id) {
        Product productFound = productService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(productFound);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Integer id) {
        productService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/category/native/{categoryId}")
    public ResponseEntity<List<Product>> getProductsByCategoryNative(@PathVariable Integer categoryId) {
        List<Product> products = productService.getProductsByCategoryNative(categoryId);
        return ResponseEntity.status(HttpStatus.OK).body(products);
    }

    @GetMapping("/category/custom/{categoryId}")
    public ResponseEntity<List<Product>> getProductsByCategoryCustomMethod(@PathVariable Integer categoryId) {
        List<Product> products = productService.getProductsByCategoryCustomMethod(categoryId);
        return ResponseEntity.status(HttpStatus.OK).body(products);
    }

    @GetMapping("/category/jpql/{categoryId}")
    public ResponseEntity<List<Product>> getProductsByCategoryJPQL(@PathVariable Integer categoryId) {
        List<Product> products = productService.getProductsByCategoryJPQL(categoryId);
        return ResponseEntity.status(HttpStatus.OK).body(products);
    }

    @GetMapping("/search")
    public ResponseEntity<List<Product>> getFiltered(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) Integer stock) {
        List<Product> productsFiltered = productService.getFiltered(name, stock);
        return ResponseEntity.status(HttpStatus.OK).body(productsFiltered);
    }

    @GetMapping("/customSearch")
    public ResponseEntity<List<ProductPojo>> customSearch(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) Integer stock) {
        ProductFilter filters = new ProductFilter(name, stock);
        List<ProductPojo> productsFiltered = productService.customSearch(filters);
        return ResponseEntity.status(HttpStatus.OK).body(productsFiltered);
    }
}
