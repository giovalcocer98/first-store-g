package et.store.api.presentation.request.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CategoryDto {
    private Integer id;
    @NotBlank(message = "{category.name.not-blank}")
    @Size(min = 3, max = 70, message = "{category.name.size}")
    private String name;

    @NotBlank(message = "{category.description.not-blank}")
    private String description;
}
