package et.store.api.presentation.request.dto;

import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProductDto {

    private Integer id;

    @NotBlank(message = "{product.name.not-blank}")
    @Size(min = 3, max = 70, message = "{product.name.size}")
    private String name;

    @NotBlank(message = "{product.description.not-blank}")
    private String description;

    @NotNull
    @DecimalMin(value = "0.0", inclusive = false, message = "{product.price.min}")
    private Double price;

    @NotNull
    @Min(value = 0, message = "{product.stock.min}")
    private Integer stock;

    @NotNull
    private Integer categoryId;
}
