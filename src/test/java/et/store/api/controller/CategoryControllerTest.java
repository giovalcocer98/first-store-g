package et.store.api.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import java.util.HashMap;
import java.util.Map;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class CategoryControllerTest {

    @BeforeAll
    public static void setup() {
        baseURI = "http://localhost:8085/store/v1";
    }

}
