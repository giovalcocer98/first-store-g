# Usa una imágen base de Java
FROM gradle:8.1-jdk17

# Directorio de trabajo dentro del contenedor
WORKDIR /app

# Copia el archivo build.gradle y settings.gradle al contenedor
COPY build.gradle .
COPY settings.gradle .

# Copia los archivos fuente Gradle
COPY gradle/ /app/gradle/

# Descarga las dependencias de Gradle
RUN gradle --no-daemon build --stacktrace

# Copia el resto del código fuente
COPY src/ /app/src/

# Compila la aplicación
RUN gradle build

# Copia el archivo JAR construido de tu aplicación (asegúrate de que ya lo has construido previamente)
# COPY build/libs/api-0.0.1-SNAPSHOT.jar app.jar

# Expone el puerto en el que se ejecuta tu aplicación
# EXPOSE 8085

# Comando para ejecutar tu aplicación Spring Boot
CMD ["java", "-jar", "build/libs/api-1.0.jar"]