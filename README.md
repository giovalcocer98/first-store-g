## First Store
### Arquitectura Presentation - Domain - Data

![presentation_domain_data](https://gitlab.et.bo/spring-boot-demos/first-store/-/raw/develop/src/main/resources/static/images/PresentationDomainDataLayering.png)

Esta arquitectura por capas busca una separación clara de responsabilidades para mejorar la mantenibilidad, la escalabilidad y la flexibilidad del proyecto. Al dividir el proyecto en estas capas, se facilita el desarrollo, el mantenimiento y la evolución del software de manera modular y organizada. Ademas de organizar estas capas en diferentes niveles funcionales, cada capa debe comunicarse con las capas adyacentes a través de interfaces bien definidas y, en lo posible, mantener un bajo acoplamiento entre ellas.

Según Martin Fowler en su libro "Patterns of Enterprise Application Architecture" bajo el contexto de "Presentation-Domain-Data Layering" el autor describe una arquitectura que organiza el código en tres capas distintas:

1. **Capa de presentación (Presentation layer):** Se encarga de la interfaz de usuario y la presentación de datos al usuario. Aquí se encuentran las vistas y los controladores que interactúan directamente con los usuarios.

2. **Capa de dominio (Domain layer):** Contiene toda la lógica de negocio y las reglas del dominio. Esta capa encapsula el conocimiento del negocio y no debe depender de detalles técnicos ni de la forma en que se almacenan los datos.

3. **Capa de datos (Data layer):** Es responsable de la persistencia de los datos y las interacciones con la base de datos u otros sistemas de almacenamiento. Aquí se encuentran los mecanismos para acceder, guardar y manipular los datos.

El presente proyecto es una demo de una tienda de productos (store) enfocado a la arquitectura anteriormente mencionada. Si bien el ejemplo tiene operaciones básicas entre categorías y productos, el mismo es un punto de partida importante para conocer la arquitectura y el enfoque que se quiere dar a los proyectos que lo vayan a utilizar.

A continuación se muestra la estructura del proyecto con sus capas y sus diferentes niveles funcionales:

```
├── src
│   ├── main
│   │   ├── java
│   │   │   ├── et.store.api
│   │   │   │   ├── config
│   │   │   │   ├── common
│   │   │   │   ├── data
│   │   │   │   │   ├── repository
│   │   │   │   ├── domain
│   │   │   │   │   ├── entity
│   │   │   │   │   ├── mapper
│   │   │   │   │   ├── service
│   │   │   │   │   ├── useCases
│   │   │   │   ├── presentation
│   │   │   │   │   ├── controller
│   │   │   │   │   ├── request
│   │   │   │   │   │   ├── dto
│   │   │   │   │   ├── response
│   │   │   │   │   │   ├── pojo
│   │   │   │   ├── exception
│   │   │   ├── ApiApplication.java
│   │   ├── resources
│   │   │   ├── properties
│   │   │   │   ├── static.postman-collection
│   │   │   │   ├── *.properties
│   ├── test
├── build.gradle
└── .gitignore
```

El autor también menciona que a pesar de la separación de datos de dominio de presentación es un enfoque común, solo debe aplicarse con una granularidad relativamente pequeña. A medida que crece una aplicación, cada capa puede volverse lo suficientemente compleja por sí sola como para que necesite modularizarla aún más. Cuando esto sucede, generalmente no es mejor usar presentación-dominio-datos como el nivel más alto de módulos. Pero una vez que cualquiera de estas capas se vuelve demasiado grande, debe dividir su nivel superior en módulos orientados al dominio que están en capas internas.

![vertical_slicing](https://gitlab.et.bo/spring-boot-demos/first-store/-/raw/develop/src/main/resources/static/images/VerticalSlicing.png)